package fzgProjekte;

public class FzgProjektEins {

	public static void main(String[] args) {
		System.out.println("Hallo Autohaus: ");
		// TEIL 1 - Java-Projekt erstellen mit Name Java_Fahrzeug - done

		// TEIL 2 - Package mit Name main einfügen - done

		// Teil 3 - Die Klasse mit den Namen Main erschaffen -done

		// TEIL 4 - Variablen mit passende Datentypen und Werte erstellen:

		// PKWs
		String car1 = "Audi";
		String car2 = "Mercedes";
		String car3 = "VW";

		// Anzahl PKWs
		String sumCars = car1 + car2 + car3;

		// LKWs
		String truck1 = "Volvo";
		String truck2 = "VW";

		// Anzahl LKWs
		String sumTrucks = truck1 + truck2;

		// Sonstige Variablen
		int price = 2000;
		double myDiscount;

		// TEIL 5 - Aussagen mit for-Schleife einfügen:

		for (int i = 1; i <= 5; i++) {
			System.out.println("Es gibt ein neues Fahrzeug im Autohaus. Anzahl Fahrzeuge: " + i);
			if (i < 5) {
				System.out.println("Es sind noch nicht alle Fahrzeuge registriert.");
			}
			if (i > 5) {
				System.out
						.println("Folgende Marken sind zu verkaufen: " + car1 + "," + car2 + "," + car3 + "," + truck1);
			}

		}

		// TEIL 6 - Weitere Aussagen implementieren:

		if (sumCars == sumTrucks) {
			System.out.println("Es gibt gleich viele LKWs wie PKWs.");
		} else {
			System.out.println("Es gibt nicht gleich viele LKWs wie PKWs.");
		}

		// Anzahl Fahrzeuge hat sich verdoppelt
		int doubleVehicles = 10;

		if (doubleVehicles == 10) {
			System.out.println("Es gibt neue Fahrzeuge im Autohaus. Anzahl Fahrzeuge: " + doubleVehicles);
		}

		// Rabatt von 0 auf 10 setzen
		double myNewDiscount = 10;

		if (myNewDiscount > 0) {
			System.out.println("Rabatt: " + myNewDiscount + "%");
		}

		// TEIL 7 - Berechnung einbauen

		switch (car1) {
			case "Audi":
				myDiscount = 5;
				myDiscount *= 5;
				myDiscount /= 2;
				myDiscount = (myDiscount > 15) ? 15 : myDiscount;
				myDiscount = (100 - myDiscount) / 100;
				System.out.println("Der Preis des ersten PKWs ist: " + (price * myDiscount));
				break;

			case "Mercedes":
				myDiscount = 5 + 10;
				myDiscount *= 5;
				myDiscount /= 2;
				myDiscount = (myDiscount > 15) ? 15 : myDiscount;
				System.out.println("Der Preis des ersten PKWs ist: " + (price * myDiscount));
				break;

			case "Vw/Volvo":
				price = (price % 2 == 0) ? 1500 : 1700;
				System.out.println("Der Preis des ersten PKWs ist: " + price);
				break;

			default:
				System.out.println(
						"Der Preis konnte nicht berechnet werden, da die Marke des Fahrzeugs nicht erkennbar ist.");
		}
	}
}
