package fahrzeuge;

import java.util.Calendar;

public class LKW extends Fahrzeuge {
	public LKW(int id, String marke, int baujahr, double grundpreis) {
		super(id, marke, baujahr, grundpreis);
	}

	@Override
	public double getRabatt() {
		int actualYear = Calendar.getInstance().get(Calendar.YEAR);
		int age = actualYear - baujahr;
		double discount = age * 6;
		if (discount > 15) {
			return 0.15;
		} else {
			return discount / 100;
		}
	}

	@Override
	public void print() {
		double discount = getRabatt();
		System.out.print("ID: " + id + "\n" + "Marke: " + marke + "\n" + "Baujahr: " + baujahr + "\n" + "Preis: "
				+ grundpreis + "\n" + "Rabatt: " + discount + "\n" + "Endpreis: " + (discount));
	}
}
