package fahrzeuge;
import java.util.Calendar;

public class PKW extends Fahrzeuge {
    private int demo;
    public PKW(int id, String marke, int baujahr, double grundpreis, int demo) {
        super(id, marke, baujahr, grundpreis);
        this.demo=demo;
    }
    @Override
    public double getRabatt() {
        int actualYear = Calendar.getInstance().get(Calendar.YEAR);
        int age = actualYear - baujahr;
        int demoYears = demo - baujahr;
        double discount = (age * 5) + (demoYears * 3);
        if (discount > 10) {
            return 0.10;
        } else {
            return discount / 100;
        }
    }
    @Override
    public void print() {
        double discount = getRabatt();
        System.out.print("ID: " + id + "\n" + "Marke: " + marke + "\n" + "Baujahr: " + baujahr + "\n" + "Vorführwagen bis: " + demo + "\n" +
                "Preis: " + grundpreis + "\n" + "Rabatt: " + discount + "\n" + "Endpreis: " + calcPrice(discount));
    }

    public int setVorfuehrwagenJahr(int jahr) {
        int actualYear = Calendar.getInstance().get(Calendar.YEAR);
        if (jahr < baujahr) {
            System.out.println("Das letzte Jahr als Vorführwagen darf nicht vor dem Baujahr liegen");
        } else if (jahr > actualYear) {
            System.out.println("Das letzte Jahr als Vorführwagen darf nicht größer als das aktuelle Jahr sein");
        } else {
            demo = jahr;
        }
        return jahr;
    }

    public int getDemo() {
        return demo;
    }

    public void setDemo(int demo) {
        this.demo = demo;
    }
}
