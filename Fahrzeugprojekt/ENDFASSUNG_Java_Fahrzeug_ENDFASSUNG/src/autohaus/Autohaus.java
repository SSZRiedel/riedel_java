package autohaus;

import fahrzeuge.*;
import java.util.ArrayList;
import java.util.HashSet;

public class Autohaus {

	ArrayList<Fahrzeuge> fahrzeugListe;
	protected int raum;
	protected String name;

	public Autohaus(int raum, String name) {
		this.fahrzeugListe = new ArrayList<Fahrzeuge>();
		this.raum = raum;
		this.name = name;
	}

	public boolean Ueberlastung(int personal) {
		if (fahrzeugListe.size() / (double) personal > 3) {
			return true;
		}
		return false;
	}

	public boolean Ueberlastung() {
		if (fahrzeugListe.size() > raum) {
			return true;
		}
		return false;
	}

	public void print() {
		System.out.println("~~~ " + name + " ~~~");
	}

	public void setFahrzeugList(String fahrzeugTyp, int id, String marke, int baujahr, int grundpreis, int demo) {
		if (fahrzeugTyp.toLowerCase().equals("pkw")) {
			fahrzeugListe.add(new PKW(id, marke, baujahr, grundpreis, demo));
		} else if (fahrzeugTyp.toLowerCase().equals("lkw")) {
			fahrzeugListe.add(new LKW(id, marke, baujahr, grundpreis));
		} else {
			System.out.println("Bitte nur PKW oder LKW eingeben.");
		}
	}

	public int FahrzeugAnzahl() {
		return fahrzeugListe.size();
	}

	public HashSet<String> getMarkes() {
		HashSet<String> markes = new HashSet<>();
		for (Fahrzeuge fahrzeug : fahrzeugListe) {
			markes.add(fahrzeug.getMarke());
		}
		return markes;
	}

	public int getRaum() {
		return raum;
	}

	public void setRaum(int raum) {
		this.raum = raum;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
