package vehicle;

// Klasse Pkw erstellt, um von Klasse Lkw zu trennen
// Eigenschaften implementieren für Pkw

public class Car {
    private int id;
    private String marke;
    private int baujahr;
    private double grundpreis;

    private int vorführwagen;

    public Car(int id, String marke, int baujahr, int grundpreis, int vorführwagen) {
        this.id = id;
        this.marke = marke;
        this.baujahr = baujahr;
        this.grundpreis = grundpreis;
        this.vorführwagen = vorführwagen;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMarke() {
        return marke;
    }

    public void setMarke(String marke) {
        this.marke = marke;
    }

    public int getBaujahr() {
        return baujahr;
    }

    public void setBaujahr(int baujahr) {
        this.baujahr = baujahr;
    }

    public double getGrundpreis() {
        return grundpreis;
    }

    public void setGrundpreis(double grundpreis) {
        this.grundpreis = grundpreis;
    }

    public int getVorführwagen() {
        return vorführwagen;
    }

    public void setVorführwagen(int vorführwagen) {
        this.vorführwagen = vorführwagen;
    }
}


// Methode getRabatt() einfügen Car
// FzgAlter berechnen + Jahre als Vorführwagen mit ein berechnen
// geg. FzgAlter pro Jahr 5% + wenn Vorführwagen pro Jahr +3%
// max. Rabatt 10% des Grundpreises (2000) (min 0%)
//        String car1="Audi";
//        String car2="Mercedes";
//        String car3="VW";

//        // Anzahl PKWs
//        int sumCars=3;
//
//        // Sonstige Variablen
//        int price=2000;
//        float myDiscount;

        // TEIL 5 - Aussagen mit for-Schleife einfügen:
//
//        for(int i=1;i<=5;i++){
//        System.out.println("Es gibt ein neues Fahrzeug im Autohaus. Anzahl Fahrzeuge: "+i);
//        if(i< 5){
//        System.out.println("Es sind noch nicht alle Fahrzeuge registriert.");
//        }
//        if(i>=5){
//        System.out.println("Folgende Marken sind zu verkaufen: "+car1+","+car2+","+car3);
//        }
//

        // Preis ohne Rabatt= 2000
        // Abzgl. FzgAlter
        // plus Jahre als VFW
        // Neuer Preis


        // Anzahl Fahrzeuge hat sich verdoppelt - für Belastungstest
//        int workersVehiclesRatio=3;
//
//        if(workersVehiclesRatio<=3){
//        System.out.print("Autohaus ist nicht belastet "+workersVehiclesRatio);
//        }
//        else{workersVehiclesRatio>3){
//        System.out.print("Autohaus ist belastet "+workersVehiclesRatio);
//
//        // Rabatt von 0 auf 10 - für neuen Preis (Rabatt + Vorführwagen)
//        float myOldDiscount=min0;
//        float myNewDiscount=max10;
//
//        if(myNewDiscount>0){
//        System.out.println("Rabatt: "+myNewDiscount+"%");
//        }

        // IF?
//        if(fzgAlter<=2){
//        rabatt="Rabat easy"
//        }else if(>2){
//        rabatt="Kein Rabatt";
//        }else{
//        rabatt=Error;
//        }
//        System.out.print(rabatt);
//        }
//
//        // TEIL 7 - Berechnungen einbauen für Vorführwagen
//        // Nach 2 jahren max. Rabatt von 10%
//        // mit Vorführwagen hängt ab wie lange:
//        //bei Jahr 1 5+3=8% Rabatt und ab Jahr 2 kommt max 10% Rabatt
//
//        switch(vorführwagen/rabatt){ // alles Null weil 0 jahre
//        case"Jahr0":
//        myDiscount=min0;
//        myDiscount=(myDiscount>10)?10:myDiscount;
//        myDiscount=(100-myDiscount)/100;
//        System.out.println("Preis mit 0 Vorführjahren: "+(price*myDiscount));
//        break;
//
//        case"Jahr1":
//        myDiscount=0+Jahre(1); //8 Prozent bei VFW oder 5Prozent ohne VFW
//        myDiscount=(myDiscount>10)?10:myDiscount;
//        System.out.println("Preis mit 1 Vorführjahren: "+(price*myDiscount));
//        break;
//
//        case"jahr2":
//        myDiscount=0+Jahre(2); // VFW fallen weg, nur noch 10% für 2 Jahre
//        myDiscount=(myDiscount>10)?10:myDiscount;
//        System.out.println("Preis mit 2 Vorführjahren: "+price);
//        break;

// Vielleicht 2 verschiedenen Methoden: eine mit und eine ohne Vorführwagen; Ergebnisse zusammenstellen
//
//default:
//        System.out.println("Der Preis konnte nicht berechnet werden, warum auch immer.");
//        }
//
//        }
//        }


// Methode die den neuen Preis angibt einfügen
// Grundpreis * (100% - berechnete %) = NeuerPreis*
// *nicht vergessen, hier beim Pkw muss Vorführjahre abgezogen werden
// Methode print() einbauen also System.out.print()

// ZUSATZ für Pkw: Methode setVorführwagen(int Jahr) einbauen, soll überprüfen,
// ob Auto ein Vorführwagen war oder nocht (true oder false)



