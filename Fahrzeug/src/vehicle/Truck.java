package vehicle;

// Klasse Lkw erstellt, um von Klasse Pkw zu trennen
// Eigenschaften implementieren für Lkw
public class Truck {

    private int id;
    private String marke;
    private int baujahr;
    private double grundpreis;

    public Truck(int id, String marke, int baujahr, int grundpreis) {
        this.id = id;
        this.marke = marke;
        this.baujahr = baujahr;
        this.grundpreis = grundpreis;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMarke() {
        return marke;
    }

    public void setMarke(String marke) {
        this.marke = marke;
    }

    public int getBaujahr() {
        return baujahr;
    }

    public void setBaujahr(int baujahr) {
        this.baujahr = baujahr;
    }

    public double getGrundpreis() {
        return grundpreis;
    }

    public void setGrundpreis(double grundpreis) {
        this.grundpreis = grundpreis;
    }


}

// Methode getRabatt() einfügen Lkw
// FzgAlter berechnen
// geg. FzgAlter pro Jahr 6%
// max. Rabatt 15% des Grundpreises (min 0%)

// Methode die den neuen Preis angibt einfügen
// Grundpreis * (100% - berechnete %) = NeuerPreis
// Methode print() einbauen also System.out.print()