package vehicle;

public class Pkw {
// Klasse Pkw erstellt, um von Klasse Lkw zu trennen
// Eigenschaften implementieren für Pkw:
// PkwID; Marke; Baujahr; Grundpreis; maxRabatt; Rabattpreis; Vorführwagen


    private int id;
    private String marke;
    private int baujahr;
    private float grundpreis;
    private float maxrabattPKW;
    private float rabattpreis;
    private int vorfuehrwagen;

    public Pkw(int id, String marke, int baujahr, float grundpreis, float maxrabattPKW, float rabattpreis, int vorfuehrwagen) {
        this.id = id;
        this.marke = marke;
        this.baujahr = baujahr;
        this.grundpreis = grundpreis;
        this.maxrabattPKW = maxrabattPKW;
        this.rabattpreis = rabattpreis;
        this.vorfuehrwagen = vorfuehrwagen;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {

        this.id = id;
    }

    public String getMarke() {

        return marke;
    }

    public void setMarke(String marke) {

        this.marke = marke;
    }

    public int getBaujahr() {

        return baujahr;
    }

    public void setBaujahr(int baujahr) {

        this.baujahr = baujahr;
    }


    public double getGrundpreis() {

        return grundpreis;
    }

    public void setGrundpreis(float grundpreis) {

        this.grundpreis = grundpreis;
    }


    public double getMaxrabattPKW() {
        return maxrabattPKW;
    }

    public void setMaxrabattPKW(float maxrabattPKW) {
        this.maxrabattPKW = maxrabattPKW;
    }


    public double getrabattpreis() {
        return rabattpreis;
    }

    public void setRabattpreis(float rabattpreis) {
        this.rabattpreis = rabattpreis;
    }

    public int getVorfuehrwagen() {
        return vorfuehrwagen;
    }

    public void setVorfuehrwagen(int vorfuehrwagen) {
        this.vorfuehrwagen = vorfuehrwagen;
    }


// Methode getRabatt() einfügen
// public Rückgabetyp methodenname(Datentyp parameter, Datentyp parameter)
// { Anweisung mit parameter; return parameter;}

//    public void getRabatt() { Anweisung Return;}

//if (fzgAlterRabattZweiJahre >= 1800 && <= 2000) {
//            System.out.println("Es gibt ein Rabatt");
//        }else {
//            System.out.println("Es gibt kein Rabatt");
//        }

//    int n = 10;
//    int sum = 0;
//        while (n > 0 ) {
//        sum += n--;
//    }
//        System.out.println("sum = " + sum);

//baujahr + vorführwagen für baujahr 5% + vorführwagen 3% beide pro jahr
//                max Rabatt 10% IF
//                min rabatt 0% IF

//    public void getRabatt() {}

//        baujahr 6% pro Jahr
//        max Rabatt 15%
//                min Rabatt 0%
//für das Fahrzeugalter pro Jahr einen Grundrabatt von 5% plus
//3% pro Jahr, in dem es ein Vorführwagen war

//    Tipp: Bevor Sie den Rabatt berechnen können, müssen Sie das Fahrzeugalter und die Anzahl
//    der Jahre, in dem es ein Vorführwagen war, berechnen.
// FzgAlter berechnen + Jahre als Vorführwagen mit ein berechnen
// geg. FzgAlter pro Jahr 5% + wenn Vorführwagen pro Jahr +3%
// max. Rabatt 10% des Grundpreises (2000) (min 0%)
// Discount = List price x Discount Rate
// Discount = 2000 * 10 / 100 = 200     Da 10 maxrabattPKW ist
// NewPrice = 2000 - Discount
// AllPackagePrice =
//
//
//    public static void main(String[] args) {
//        int number1 = 2000;
//        int number2 = 10;
//
//        int discCalc = discCalc(number1, number2);
//        ausgabe(discCalc, "discCalc()");
//    }
//
//    public static int discCalc(int number1, int number2) {
//
//       int result = number1 * number2 / 100;
//
//        return result;
//
//    }
//
//    public static void ausgabe(int result, String s){
//        System.out.println("Ergebnis " + result);
//    }


}
