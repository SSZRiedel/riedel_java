package main;


public class Main {

    public static void main(String[] args) {

// Sinnvolle Java-Klassen erstellen und in packages einfügen

// Java-Klasse Autohaus erstellen,
// Autohaus: Anzahl an Fahrzeuge, Anzahl Abstellplätze (Überlastet: j/n?)
// Autohaus Überlastung (je eine Methode erstellen)
// -Jeder Mitarbeiter kann max. 3 Fzg betreuen. Wenn mehr als 3 Fzg = Autohaus überlastet
// -Mehr Fzg als Abstellplätze = Autohaus überlastet/
// Methode print() einfügen: spukt "Name des Autohaus" aus

        System.out.println("Autohaus_TEST");

        int price = 2000;
        int maxDisc = 90;
        int divide = 100;

        int einJahr = 95;
        int vorführJahr = 97;
        int addJahre = 92;

        int mulMax = price * maxDisc / divide;
        int annoDisc = price * einJahr / divide;
        int annoVorführJahr = price * vorführJahr / divide;
        int addDisc = price * addJahre / divide;

        System.out.println("Autopreis nach abzg. max. Rabatt: " + mulMax);
        System.out.println("Autopreis nach abzg. ein Jahr Rabatt " + annoDisc);
        System.out.println("Autopreis nach abzg. ein Jahr Vorführwagen " + annoVorführJahr);
        System.out.println("Autopreis nach abzg. ein Jahr Rabatt plus ein Jahr Vorführwagen " + addDisc);


        int zweiJahr = 90;
        int vorführJahrZwei = 94;
        int addZahlZwei = 84;

        int annoDisc2 = price * zweiJahr / divide;
        int annoVorführJahrZwei = price * vorführJahrZwei / divide;
        int addDiscoZwei = price * addZahlZwei / divide;

        System.out.println("Autopreis nach abzg. zwei Jahren Rabatt " + annoDisc2);
        System.out.println("Autopreis nach abzg. zwei Jahren Vorführwagen " + annoVorführJahrZwei);
        System.out.println("Autopreis nach abzg. zwei Jahren Rabatt plus zwei Jahre Vorführwagen " + addDiscoZwei);
        // Letzter Wert geht unter 1800, also erhält es 1800 Rabatt

        int dreiJahr = 85;
        int vorführJahrDrei = 91;

        int annoDisc3 = price * dreiJahr / divide;
        int annoVorführJahrDrei = price * vorführJahrDrei / divide;

        System.out.println("Autopreis nach abzg. drei Jahren Rabatt " + annoDisc3);
        // Erster Wert fliegt auch raus bzw. KäuferIn zahlt 1800
        System.out.println("Autopreis nach abzg. drei Jahren Vorführwagen " + annoVorführJahrDrei);

        int vierJahr = 80;
        int annoDisc4 = price * vierJahr / divide;

        System.out.println("Autopreis nach abzg. vier Jahren Rabatt " + annoDisc4);
        System.out.println("");

        getRabatt();
        int maxDiscounter = 200;
        if (maxDiscounter <= 200) {
            System.out.println("Rabatt");
        } else if (maxDiscounter >= 201) {
            System.out.println("Kein Rabatt");
        } else {
            System.out.println("Fehler");
        }

// Autopreis 2000
// Rabatt mit for schleife?
        float preis = 2000f;
//        float i;
        float total = 0;
        float discount;
        if (preis < 2001f) {
            discount = preis * 0.05f;
            total = preis - discount;
            System.out.println("Preis ohne Rabatt: " + preis + " discount= " + discount + " total= " + total);
        }

    // Bis 10 zählen
    for (int i = 1; i <= 10; i++){
        System.out.println(i);

    }
    // 2n = Even Numbers = 0, 2, 4...
    // 2m+1 = Odd Numbers = 1,3,5, ...
    int m = 5;
        int n = 6;
    for (int i = 0; i<1; i++){
        System.out.println(2*m+1);
        System.out.println(2*n);
    }
    // ALso rechnung vereinfachen

    }

    public static void getRabatt() {
        int maximalerRabatt = 2000 * 10 * 1 / 100;
        int mininmalerRabatt = 0;
        int fzgAlterRabattEinJahr = 2000 * 5 * 1 / 100;
        int fzgAlterRabattZweiJahre = 2000 * 5 * 2 / 100;
        int vorführwagenRabattEinJahr = 2000 * 3 * 1 / 100;
        int vorführwagenRabattZweiJahre = 2000 * 3 * 2 / 100;
        int voführwagenRabattDreiJahre = 2000 * 3 * 3 / 100;


        System.out.println("Rabattberechnung: " + maximalerRabatt);
        System.out.println("Minimalrabatt: " + mininmalerRabatt);
        System.out.println("Ein Jahresrabatt: " + fzgAlterRabattEinJahr);
        System.out.println("Zwei Jahresrabatt: " + fzgAlterRabattZweiJahre); // mehr nicht drinne
        System.out.println("Ein Vorführrabatt: " + vorführwagenRabattEinJahr);
        System.out.println("Zwei Vorführrabatt: " + vorführwagenRabattZweiJahre);
        System.out.println("Drei Vorführrabatt: " + voführwagenRabattDreiJahre);

// jetzt alles miteinadner fusionieren, um eine Methode herauszubekommen

    }
}


//    float cost = 19000f, i, total = 0, discount = 0;
//        if (cost < 20000) {
//        discount = cost * 0.05f;
//        total = cost - discount;
//        System.out.println("discount= " + discount + " total= " + total);
//
//    }






